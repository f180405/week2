package com.example.week2;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button btn;
    EditText username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn = findViewById(R.id.button);
        username = (EditText) findViewById(R.id.editTextTextPersonName);
//        password = findViewById(R.id.editTextTextPassword);

        btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v){
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this );
                if (username.getText().toString().equals("F180405"))
                    Toast.makeText(getApplicationContext(), "User authenticated", Toast.LENGTH_LONG).show();
                else {
                    builder .setTitle("Error")
                            .setMessage("The given username is invalid!")
                            .setNegativeButton("Close", null)
                            .show();
                }
            }
        });
    }
}